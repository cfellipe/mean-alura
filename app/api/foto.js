var api = {};// criação do objeto 
var mongoose = require('mongoose');// requerindo mogoose;
var model= mongoose.model('Foto')// solicitando o modelo 'Foto'

api.lista = function(req, res){// adiciona dinamicamente a propriedade `lista` associando uma função
    
    model.find({})
    .then(function(fotos){
        res.json(fotos);
    }, function(error){
    console.log(error);
        res.send(500).json(error);
    })
};

api.buscaPorId = function(req,res){
  
 model.findById(req.params.id)
    .then(function(foto){
     if(!foto) throw Error ('foto nao encontrada')// lança uma exceção caso a foto nao for encontrada
     res.json(foto)
 }, function(erro){
     console.log(erro);
     res.send(404).json(erro)
 })
    
}

api.removePorId = function(req, res){
  model.remove({_id: req.params.id})
      .then(function(foto){
      res.sendStatus(204);
      console.log('foto '+ foto.titulo + ' removida com sucesso');
  }, function(erro){
      console.log(error);
      res.send(500).json(error);
  })
}

api.adiciona = function(req,res){
    
  model.create(req.body)
      .then(function(foto){
      res.json(foto)
      console.log('Foto '+ foto.titulo + (' adicionada com sucesso'));
  }, function(erro){
      console.log(error);
      res.send(500).json(error);
  })
}

api.atualiza = function(req, res){

    model.findByIdAndUpdate(req.params.id, req.body)
        .then(function(foto){
        res.json(foto)
        console.log('Foto '+ foto.titulo + (' Atualizada com sucesso'));
    }, function(erro){
        console.log(error);
        res.send(500).json(error);
    })
}
module.exports = api;