module.exports = function(app) {//exportando o modulo para o consign 
    
    var api = app.api.foto;// como ja foi configurado no consign, foi passado o direterio dessa forma para a variavel.
    
    //rotas
    app.route('/v1/fotos')
        .get(api.lista)
        .post(api.adiciona);
    
    app.route('/v1/fotos/:id')
        .get(api.buscaPorId)
        .delete(api.removePorId)
        .put(api.atualiza);
    
   
    
};