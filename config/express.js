var express = require('express');//requerindo o express da pasta node_modules
var app = express();//instanciadno o expess
var consign = require('consign');//requerindo o consign da pasta node-modules
var bodyParse= require('body-parser');//requerido o bodyParse

app.use(express.static('./public'))// middleware
app.use(bodyParse.json());// configurando o middleware body-parser


consign({cwd:'app'})
    .include('models')
    .then('api')
    .then('routes')
    .into(app);//realiza o carregamento dos arquivos localizados nesse diretorio e add a instancia do express configurada"app"


module.exports = app; // exportando a instancia do express para o server
