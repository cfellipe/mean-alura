module.exports = function (uri){//realizando a exportação
    
    var mongoose = require('mongoose');//requerindo o mogoose
    
    mongoose.connect('mongodb://' + uri);//realizando a conexao juntamente com o banco
    
    
    //apresentando mensagem para o servidor
    mongoose.connection.on('connected', function(){
        
        console.log('Conectado ao MongoDB');
        
    });
    
    mongoose.connection.on('error', function(error){
        
        console.log('Erro no DB'+ error);
    })
    
    mongoose.connection.on('disconnected', function(){
        
        console.log('Mongo Desconectado');
    })
    
    process.on('SIGINT', function (){
        
        mongoose.connection.close(function(){
            console.log('conexao fechada');
            process.exit(0);// Usamos process.exit(0) para indicar que foi um término de aplicação esperado, não decorrente de um erro.
        })
        
    })
}